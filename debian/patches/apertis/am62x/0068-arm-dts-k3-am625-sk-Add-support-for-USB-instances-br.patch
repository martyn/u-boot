From: Aswath Govindraju <a-govindraju@ti.com>
Date: Wed, 6 Apr 2022 20:54:45 +0530
Subject: arm: dts: k3-am625-sk: Add support for USB instances brought out on
 the board

The two instance of USB IP in AM62 SoC are brought out on the SK board. The
zeroth instance is brought out through a USB TYPE C port and the first
instance through a USB TYPE A port. As role switching is supported in
U-Boot and dfu support is required, fix the role of zeroth instance to
peripheral in the U-Boot dts file.

Signed-off-by: Aswath Govindraju <a-govindraju@ti.com>
---
 arch/arm/dts/k3-am625-sk-u-boot.dtsi | 23 +++++++++++++++++++++++
 arch/arm/dts/k3-am625-sk.dts         | 35 +++++++++++++++++++++++++++++++++++
 2 files changed, 58 insertions(+)

diff --git a/arch/arm/dts/k3-am625-sk-u-boot.dtsi b/arch/arm/dts/k3-am625-sk-u-boot.dtsi
index f275e3b..ad4ef27 100644
--- a/arch/arm/dts/k3-am625-sk-u-boot.dtsi
+++ b/arch/arm/dts/k3-am625-sk-u-boot.dtsi
@@ -148,3 +148,26 @@
 &cpsw_port2 {
 	status = "disabled";
 };
+
+&usbss0 {
+	u-boot,dm-spl;
+};
+
+&usb0 {
+	dr_mode = "peripheral";
+	/* Since role switching is not supported in U-Boot */
+	/delete-property/ extcon;
+	u-boot,dm-spl;
+};
+
+&usbss1 {
+	u-boot,dm-spl;
+};
+
+&usb1 {
+	u-boot,dm-spl;
+};
+
+&main_usb1_pins_default {
+	u-boot,dm-spl;
+};
diff --git a/arch/arm/dts/k3-am625-sk.dts b/arch/arm/dts/k3-am625-sk.dts
index af5617f..899883a 100644
--- a/arch/arm/dts/k3-am625-sk.dts
+++ b/arch/arm/dts/k3-am625-sk.dts
@@ -24,6 +24,8 @@
 		spi0 = &ospi0;
 		ethernet0 = &cpsw_port1;
 		ethernet1 = &cpsw_port2;
+		usb0 = &usb0;
+		usb1 = &usb1;
 	};
 
 	chosen {
@@ -143,9 +145,22 @@
 			default-state = "off";
 		};
 	};
+
+	extcon_usb0: extcon-usb0 {
+		compatible = "linux,extcon-usb-gpio";
+		id-gpios = <&main_gpio1 50 GPIO_ACTIVE_HIGH>;
+		pinctrl-names = "default";
+		pinctrl-0 = <&extcon_usb0_gpio_id_pins_default>;
+	};
 };
 
 &main_pmx0 {
+	extcon_usb0_gpio_id_pins_default: extcon-usb0-gpio-id-pins-default {
+		pinctrl-single,pins = <
+			AM62X_IOPAD(0x254, PIN_INPUT_PULLUP, 7) /* (C20) USB0_DRVVBUS.GPIO1_50 */
+		>;
+	};
+
 	main_uart0_pins_default: main-uart0-pins-default {
 		pinctrl-single,pins = <
 			AM62X_IOPAD(0x1c8, PIN_INPUT, 0) /* (D14) UART0_RXD */
@@ -275,6 +290,12 @@
 			AM62X_IOPAD(0x01d4, PIN_INPUT, 7) /* (B15) UART0_RTSn.GPIO1_23 */
 		>;
 	};
+
+	main_usb1_pins_default: main-usb1-pins-default {
+		pinctrl-single,pins = <
+			AM62X_IOPAD(0x0258, PIN_OUTPUT, 0) /* (F18) USB1_DRVVBUS */
+		>;
+	};
 };
 
 &wkup_uart0 {
@@ -388,6 +409,20 @@
 	disable-wp;
 };
 
+&usbss0 {
+	ti,vbus-divider;
+};
+
+&usb0 {
+	extcon = <&extcon_usb0>;
+};
+
+&usb1 {
+	dr_mode = "host";
+	pinctrl-names = "default";
+	pinctrl-0 = <&main_usb1_pins_default>;
+};
+
 &cpsw3g {
 	pinctrl-names = "default";
 	pinctrl-0 = <&main_mdio1_pins_default
